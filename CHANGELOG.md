# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

## [0.9.12] - 2019-04-25
### Changed
- Upgraded mysql to 8.0.16 and relinked .NET property nodes, methods and constructors

## [0.9.11] - 2019-04-25
### Changed
- Updated @cs/database and @cs/assertion dependencies
- Recompiled project to fix linking issues

## [0.9.10] - 2019-04-23
### Changed
- Updated @cs/database and @cs/assertion dependencies

## [0.9.9] - 2019-03-05
### Changed
- Updated @cs/database dependency
- added support for binary data as string

## [0.9.8] - 2019-03-05
### Changed
- Updated @cs/database dependency
- Implemented new malleable database API 

## [0.9.7] - 2019-02-26
### Changed
- Updated @cs/database dependency to version 1.1
- Updated mysql to 8.0.15

## [0.9.6] - 2019-02-26
### Added
- Everything (Initial publication)